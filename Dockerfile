FROM python:3.12-slim

MAINTAINER Graeme Moss
ADD requirements.txt /home/requirements.txt
ADD azirevpn.py /home/azirevpn.py
RUN pip3 install --no-cache-dir -r /home/requirements.txt

CMD ["/home/azirevpn.py"]
ENTRYPOINT ["python3"]