# azirevpn-deluge-sidecar

I wrote this as azirevpn had moved away from openvpn open port setup. I wanted to understand the api setup and how to get a port forwarding working sadly the API is not rich in features but hopefully it will get better. for now this works to get a port open and checks it when it restarts. 

I mainly use K8s but you can deploy this in a docker-compose setup 

```
version: "3.7"

services:
  deluge:
    image: linuxserver/deluge
    restart: unless-stopped
    network_mode: service:gluetun
    volumes:
      - ./data/deluge/config:/config
      - downloads-data:/data
    environment:
     # my volume is cifs and expects this
      - PUID=1000
      - GUID=1000

  portforward:
    image: registry.gitlab.com/grami.moss/azirevpn-deluge-sidecar:latest
    network_mode: service:gluetun
    environment:
      - AZIREVPN_TOKEN=aabbccddeeffgg
      # gluetun uses tun0 change to wg0 is you need to
      - AZIREVPN_INTERFACE=tun0
      - DELUGE_HOST=127.0.0.1
      - DELUGE_USERNAME=localclient
      - DELUGE_PASSWORD=700dc5cbf758e682db1724f951619992173fd172

  gluetun:
    image: qmcgaw/gluetun:latest
    container_name: gluetun-torrent
    cap_add:
      - NET_ADMIN
    environment:
      - TZ=America/Chicago
      - VPN_SERVICE_PROVIDER=xxxxx
      - VPN_TYPE=wireguard
      - WIREGUARD_PRIVATE_KEY=xxxxx
      - WIREGUARD_PRESHARED_KEY=xxxxx
      - WIREGUARD_ADDRESSES=10.1.1.1/32, xxxxx
      - SERVER_REGIONS=America,Asia,Europe,Oceania
      # Allow access from local subnet and VPN subnet
      - FIREWALL=off
    ports:
      - 8112:8112/tcp
      - 12345:12345/tcp # whatever you setup port forwarding for
      - 12345:12345/udp

volumes:
  downloads-data:
```

here is my k8s deployment example which is copied from the docker-compose but with extra configurations to support firewalls.

```
---

apiVersion: v1
kind: ConfigMap
metadata:
  name: gluetun-iptables
data:
  post-rules.txt: |
    iptables -A INPUT -i tun0 -s 0.0.0.0/0 -p udp -m multiport --dports 52000:65535 -j ACCEPT
    iptables -A INPUT -i tun0 -s 0.0.0.0/0 -p tcp -m multiport --dports 52000:65535 -j ACCEPT

---
 apiVersion: apps/v1
 kind: Deployment
 metadata:
   name: gluetun-deployment
   labels:
     app: gluetun
 spec:
   replicas: 1
   strategy:
       type: RollingUpdate
       rollingUpdate:
          maxSurge: 1
   selector:
      matchLabels:
        app: gluetun
   template:
     metadata:
       labels:
         app: gluetun
     spec:
      containers:
         - name: gluetun
           image: ghcr.io/qdm12/gluetun # Optionally you can use the "qmcgaw/gluetun" image as well as specify what version of Gluetun you desire
           imagePullPolicy: Always
           securityContext:
             capabilities:
               add: ["NET_ADMIN"]
           # You can optionally specify ports that are used by your containers, but it is not strictly necessary
           # ports:
             # - containerPort: 8080

           # Adapt the following environment variables to suit your needs and VPN provider
           env:
           - name: TZ
             value: "America/Chicago"

           - name: VPN_SERVICE_PROVIDER
             value: "custom"

           - name: VPN_TYPE
             value: "wireguard"

           - name: VPN_ENDPOINT_IP
             value: "45.15.19.11"

           - name: VPN_ENDPOINT_PORT
             value: "51820"
          
           - name: WIREGUARD_PUBLIC_KEY
             value: "lksjgaklsdjgklasdjgkldsajg"

           - name: WIREGUARD_PRIVATE_KEY
             value: "skjaghjkasdhgjghksdj"

           - name: WIREGUARD_ADDRESSES
             value: "10.0.88.1/32"

           - name: VPN_PORT_FORWARDING
             value: "off"

           volumeMounts:
             - name: iptables
               mountPath: /iptables
           #  - name: gluetun-config
           #    mountPath: /gluetun


         - name: portforward
           image: registry.gitlab.com/grami.moss/azirevpn-deluge-sidecar:latest
           env:
           - name: AZIREVPN_TOKEN
             value: "aabbccddeeffgghh"

         - name: deluge
           image: linuxserver/deluge:latest
           volumeMounts:
             - name: deluge
               mountPath: /config
           ports:
            - containerPort: 8112 

      volumes:
        - configMap:
            name: gluetun-iptables
          name: iptables
        - name: deluge
          hostPath:
            path: /root/gluetun/deluge
#        - name: gluetun-config
#          persistentVolumeClaim:
#            claimName: pvc-gluetun-config

---
apiVersion: v1
kind: Service
metadata:
  name: deluge-service
  namespace: default
  annotations:
    metallb.universe.tf/loadBalancerIPs: 10.17.0.4
spec:
  selector:
    app: gluetun
  ports:
    - port: 8112
      targetPort: 8112
  type: LoadBalancer
  externalTrafficPolicy: Local

```