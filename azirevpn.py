import requests
import json
import os
import logging
import ifaddr
import signal

from deluge_client import DelugeRPCClient


logging.basicConfig()
log = logging.getLogger('azirevpn')
log.setLevel(logging.DEBUG)


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token
    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + self.token
        return r

'''
check the ip for any ports
if port listed check if open 
else if no port create port and update deluge
'''

# Get Variables from the container
token = os.environ.get('AZIREVPN_TOKEN')
tunnelInterface = os.environ.get('AZIREVPN_INTERFACE', 'tun0')
delugeHost = os.environ.get('DELUGE_HOST', '127.0.0.1')
delugePort = os.environ.get('DELUGE_PORT', 58846)
delugeUser = os.environ.get('DELUGE_USERNAME', 'localclient')
delugePassword = os.environ.get('DELUGE_PASSWORD', '700dc5cbf758e682db1724f951619992173fd172')



def get_ip_for_interface(interface_name, ipv6=False):
        """
        Get the ip address in IPv4 or IPv6 for a specific network interface

        :param str interface_name: declares the network interface name for which the ip should be accessed
        :param bool ipv6: Boolean indicating if the ipv6 address should be retrieved
        :return: IPv4Address or IPv6Address object or None
        """

        def get_interface_by_name(name):
            for interface in ifaddr.get_adapters():
                if interface.name == name:
                    return interface
            return None

        interface = get_interface_by_name(interface_name)

        if interface is None:
            return None

        for ip in interface.ips:
            if ip.is_IPv6 and ipv6:
                return ip.ip[0]  # first of (ip, flowinfo, scope_id) tuple
            if ip.is_IPv4 and not ipv6:
                return ip.ip

        return None 


try:
    #ip = os.popen(f'ip addr show {tunnelInterface}').read().split("inet ")[1].split("/")[0]
    #ip = ni.ifaddresses(tunnelInterface)[ni.AF_INET][0]['addr']
    ip = get_ip_for_interface(tunnelInterface)
except:
    exit(1)

port = 0

log.info(f'Got the following ip {ip}')
response = requests.get(f'https://api.azirevpn.com/v3/portforwardings?internal_ipv4={ip}', auth=BearerAuth(token))
if response.status_code == 200:
    data = json.loads(response.text)
    if data['status'] == "success":
        if len(data['data']['ports']) == 1:
            port = data['data']['ports'][-1]['port']
            log.info(f'Found existing port {port}')
            # TODO: API doesn't have a clear way to see if the port is closed only open which means we can't direct if closed.
        elif len(data['data']['ports']) == 0:
            
            payload = {
                "internal_ipv4": ip,
                "hidden": "false",
                "expires_in": 0
            }
            response = requests.post(f'https://api.azirevpn.com/v3/portforwardings', auth=BearerAuth(token), json=payload)
            data = json.loads(response.text)
            if data['status'] == "success":
                port = data['data']['port']
            log.info(f"Creating new port {port}")

#Update Deluge with the port

client = DelugeRPCClient(delugeHost, delugePort , username=delugeUser, password=delugePassword)
client.connect()

client.core.set_config({"listen_ports":[port, port],"random_port":False})
log.info('Setting the listen_port and disabling random_port')

torrents = client.core.get_session_state()
if len(torrents) > 0:
    client.core.force_reannounce(torrents)
    log.info('forcing reannounce on all torrents')


signal.pause()